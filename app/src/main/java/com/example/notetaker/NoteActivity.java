package com.example.notetaker;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.provider.ContactsContract;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class NoteActivity extends AppCompatActivity {

    public static final String NOTE_INFO= "com.example.notetaker.NOTE_INFO";
    private NoteInfo mNote;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Spinner spinner_courses = findViewById(R.id.spinner_courses);

        //populate the spinner
         List<CourseInfo> Courses = DataManager.getInstance().getCourses();
        ArrayAdapter<CourseInfo> adapterCourses =
                new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, Courses);

        adapterCourses.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        spinner_courses.setAdapter(adapterCourses);

        //read the  parcelable data passed by the intent
        readDisplayState();

        EditText EditNoteTitle = findViewById(R.id.text_note_title);
        EditText EditNoteText  = findViewById(R.id.text_note_text);

        //display the data from the intent in the note activity
        displayNote(spinner_courses, EditNoteTitle, EditNoteText);


    }



    private void readDisplayState() {
        Intent intent = getIntent();
        mNote = intent.getParcelableExtra(NOTE_INFO);
    }

    public void displayNote(Spinner spinner, EditText editNoteTitle, EditText editNoteText){
        List<CourseInfo> CourseList = DataManager.getInstance().getCourses();
            int CourseIndex = CourseList.indexOf(mNote.getCourse());
            spinner.setSelection(CourseIndex);
        editNoteTitle.setText(mNote.getTitle());
        editNoteText.setText(mNote.getText());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
